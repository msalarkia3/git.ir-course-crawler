# import urllib.request
import requests
from bs4 import BeautifulSoup
from time import sleep
import os


def get_downloadlinks(page):
    response_okay = False
    while not response_okay:
        try:
            response = requests.get(page, timeout=20)
            cookies = response.headers['Set-Cookie']
            soup = BeautifulSoup(response.text, 'html.parser')
            csrftoken = soup.select_one('[name="csrf_token"]').get('content')
            links = requests.post(page, headers={
                "Referer": page,
                "Accept": "text/html, */*; q=0.01",
                "Content-Type": 'application/x-www-form-urlencoded; charset=UTF-8',
                "Cookie": cookies

            }, data={
                'csrfmiddlewaretoken': csrftoken,
                'show_download_links': 1
            }, timeout=20)

            soup = BeautifulSoup(links.text, 'html.parser')
            return [link.get('href') for link in soup.select('a')]
        except Exception as e:
            print(e, ' ====> doing it again! \n')
            continue


def get_downloadlinks_simple(page):
    response = requests.get(page)
    soup = BeautifulSoup(response.text, 'html.parser')
    download_anchoros = soup.select('.course-lecture-list > ul > li > div > a')
    links = [a.get('href') for a in download_anchoros]
    return links


def download_links(page, folder):
    links = get_downloadlinks_simple(page)
    if not os.path.exists(f'./{folder}'):
        os.makedirs(folder)
    index = 0
    while index < len(links):
        link = links[index]
        filename = ' '.join(link.split('/')[-1].split('%20'))
        filepath = f"./{folder}/{filename}"
        if os.path.exists(filepath):
            index += 1
            print(f'file {filepath} already exists!')
            continue

        print(f'file {filename} started!')

        try:
            headers = {
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36',
                'Origin': 'https://git.ir',
                'Accept': '*/*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Sec-Fetch-Site': 'same-site',
                'Upgrade-Insecure-Requests': '1',
                'Sec-Fetch-User': '?1',
                'Sec-Fetch-Mode': 'no-cors',
                'Pragma': 'no-cache',
                'Host': 'cdn9.git.ir',
                'Connection': 'keep-alive',
                'Referer': '%20'.join(link.split()),
            }
            myfile = requests.get(link)
        except Exception:
            print('doing it again!!!')
            continue

        with open(filepath, 'wb') as f:
            f.write(myfile.content)

        sleep(2)
        print(f'file {filename} downloaded!\n')
        index += 1


page = 'https://git.ir/skillshare-master-opencv-computer-vision-programming-skills-with-python3-with-face-recognition-and-ocr/'
folder = 'skillshare-master-opencv-computer-vision-programming-skills-with-python3-with-face-recognition-and-ocr'

download_links(page, folder)
